
##
$PropertyFilePath="fps-app-ws-posunificado\src\farmaventa.properties"
#$PropertyFilePath="farmaventa.properties"
$RawProperties=cat $PropertyFilePath;
$PropertiesToConvert=($RawProperties -replace '\\','\\') -join [Environment]::NewLine;
$Properties=ConvertFrom-StringData $PropertiesToConvert;
#Write-Host $Properties.version

$version = $Properties.version
$compilacion = $Properties.compilacion

$ptoventa = "mundial265"
$jarname = (Get-Item fps-app-ws-posunificado/build/libs/*.jar).Name

##
$TempFile = "actualizar_version.bat"
New-Item $TempFile -Force

echo @"
rem for /f "skip=4 tokens=1" %%a in ('net files') do net files %%a /close
sqlplus /nolog @cambios_bd.sql
rem for /f "skip=4 tokens=1" %%a in ('net files') do net files %%a /close

move /y .\$jarname    D:\pos_unificado\PtoVenta\$jarname
move /y .\POSunificado.bat    D:\pos_unificado\POSunificado.bat
"@  | Out-File -FilePath $TempFile -Encoding ASCII -Force

##
$TempFile = "actualizar_version.sh"
New-Item $TempFile -Force

echo @"
. ~/.bash_profile
export NLS_LANG=SPANISH_PERU.WE8MSWIN1252
sqlplus /nolog @cambios_bd.sql

\cp -f $jarname    /Pos_Unificado/PtoVenta/$jarname
\cp -f POSunificado.bat    /Pos_Unificado/POSunificado.bat
"@  | Out-File -FilePath $TempFile -Encoding ASCII -Force

##
$TempFile = "cambios_bd.sql"
New-Item $TempFile -Force

echo @"
-- Connect sys/namego@XE as sysdba
-- @scripts/kill_farmaventa_id.sql
Connect ptoventa/$ptoventa@XE
Set define off
@Script1.sql
quit
"@  | Out-File -FilePath $TempFile -Encoding ASCII -Force

##
$TempFile = "Script1.sql"
New-Item $TempFile -Force

echo @"
@06_ControlVersion.sql
"@  | Out-File -FilePath $TempFile -Encoding ASCII -Force

##
$TempFile = "06_ControlVersion.sql"
New-Item $TempFile -Force

echo @"
update ptoventa.rel_aplicacion_version
set flg_permitido = 0;
insert into ptoventa.rel_aplicacion_version(cod_aplicacion,num_version,cod_usuario,fch_implementacion,flg_permitido)
values('01','$version $compilacion','TEAMGN',sysdate,1);
commit;
/
"@  | Out-File -FilePath $TempFile -Encoding ASCII -Force

##
$TempFile = "POSunificado.bat"
New-Item $TempFile -Force

echo @"
@echo off
set /p servidor=Ingrese IP del servidor:
rem echo %servidor%

SET RUTA=\\%servidor%\MiFarma
set farmalibpath=%RUTA%\PtoVenta\$jarname
%RUTA%\jre7\bin\java.exe -cp %farmalibpath% mifarma.ptoventa.EconoFar %RUTA%\PtoVenta\conf\PtoVentaServ.properties %RUTA%\PtoVenta\conf\ptoventaid.properties %RUTA%\PtoVenta\conf\PtoVentaServRemotos.properties
if %USERNAME%==soporte c:\windows\explorer.exe
if %USERNAME%==Administrator c:\windows\explorer.exe
if %USERNAME%==oracle c:\windows\explorer.exe
if %USERNAME%==ventas  shutdown -l
if %USERNAME%==auditor  shutdown -l

"@ | Out-File -FilePath $TempFile -Encoding ASCII -Force
